# README #

SupportPay Axure Files

### What is this repository for? ###

* Axure Libraries to be used for mockups
* Axure mockups
* Image files
* Additional Axure files as necessary

### Files and Definitions ###

* Make sure you are running Axure 7.0
* supportpay-bootstrap-3: Axure library specifically for SupportPay product as defined by the following templates: http://supportpay.bitbucket.org


### Who do I talk to? ###

* Sheri Atwood sheri.atwood@supportpay.com