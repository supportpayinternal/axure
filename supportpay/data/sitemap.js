﻿var sitemap = 

(function() {
    var _ = function() { var r={},a=arguments; for(var i=0; i<a.length; i+=2) r[a[i]]=a[i+1]; return r; }
    var _creator = function() { return _(b,[_(c,d,e,f,g,h),_(c,i,e,f,g,j),_(c,k,e,f,g,l),_(c,m,e,f,g,n,o,[_(c,p,e,f,g,q)]),_(c,r,e,f,g,s,o,[_(c,t,e,f,g,u,o,[_(c,v,e,f,g,w,o,[_(c,x,e,f,g,y)])])]),_(c,z,e,f,g,A,o,[_(c,B,e,f,g,C,o,[_(c,D,e,f,g,E),_(c,F,e,f,g,G),_(c,H,e,f,g,I)])]),_(c,J,e,f,g,K),_(c,L,e,f,g,M,o,[_(c,N,e,f,g,O)]),_(c,P,e,f,g,Q,o,[_(c,R,e,f,g,S)]),_(c,T,e,f,g,U),_(c,V,e,f,g,W)]);}; 
var b="rootNodes",c="pageName",d="Login",e="type",f="Wireframe",g="url",h="Login.html",i="Home",j="Home.html",k="Summary",l="Summary.html",m="To Do",n="To_Do.html",o="children",p="To Do - Blank",q="To_Do_-_Blank.html",r="Add Transaction",s="Add_Transaction.html",t="Add Transaction part 2",u="Add_Transaction_part_2.html",v="Add Transaction - Completed",w="Add_Transaction_-_Completed.html",x="Add Transaction Blank",y="Add_Transaction_Blank.html",z="Review  and pay Transaction",A="Review__and_pay_Transaction.html",B="Payment Cash",C="Payment_Cash.html",D="Payment Check",E="Payment_Check.html",F="Payment-CreditCard",G="Payment-CreditCard.html",H="Payment paypal",I="Payment_paypal.html",J="Settings",K="Settings.html",L="Merchant - Saved",M="Merchant_-_Saved.html",N="Add\/Edit Merchant",O="Add_Edit_Merchant.html",P="Recipient- Saved",Q="Recipient-_Saved.html",R="Add\/Edit Recipient",S="Add_Edit_Recipient.html",T="All Transactions",U="All_Transactions.html",V="Menu Options",W="Menu_Options.html";
return _creator();
})();
